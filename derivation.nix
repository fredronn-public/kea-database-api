{ python3Packages }: with python3Packages;
let
  sqlalchemy2-stubs = (buildPythonPackage rec {
    pname = "sqlalchemy2-stubs";
    version = "0.0.2a34";
    src = fetchPypi {
      inherit pname version;
      sha256 = "sha256-JDITerL94aYI30VE9nEkJ7C3/yWZDPu8Wp0dtsjG9Ik=";
    };
    doCheck = false;
    propagatedBuildInputs = with pkgs.python310Packages; [
        sqlalchemy mypy
    ];
  });

  sqlmodel = (buildPythonPackage rec {
    pname = "sqlmodel";
    version = "0.0.8";
    src = fetchPypi {
      inherit pname version;
      sha256 = "sha256-M3G00a1Z0v/QxTBYLCFAtsBrCQsyr5ucZBKYbXsRcDY=";
    };
    doCheck = false;
    propagatedBuildInputs = with pkgs.python310Packages; [
        sqlalchemy
        sqlalchemy2-stubs
        pydantic
    ];
  });
  macaddress = (buildPythonPackage rec {
    pname = "macaddress";
    version = "2.0.2";
    src = fetchPypi {
      inherit pname version;
      sha256 = "sha256-FADM3CjXRxAtV65h5beNiYWHKTCBDOuIYM1Jq9Hh+jc=";
    };
    doCheck = false;
    propagatedBuildInputs = with pkgs.python310Packages; [
    ];
  });

in
buildPythonApplication {
    name = "kea-database-api";
    src = ./.;
    doCheck = false;
    propagatedBuildInputs = with pkgs.python310Packages; [
        pip
        requests
        sqlalchemy
        psycopg2
        uvicorn
        pydantic
        fastapi
        sqlmodel
        typer
        macaddress
        jinja2
        colorama
        rich
        shellingham
    ];
}
