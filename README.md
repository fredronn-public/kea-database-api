# kea-database-api

A kea API to interact with a backend database to be able to configure kea dhcp(4) server. Will make use of FastAPI, Pydantic and SQLAlchemy to do the heavy lifting. 

Kea: https://www.isc.org/kea/

## Running

The `chart` folder contains a helmchart, see `values.yaml` for help on using the chart.

### docker

The image is available here:

```
registry.gitlab.com/fredronn-public/kea-database-api:main
```

It needs to run using the entrypoint `kea-database-api`. The environment variable `KEA_DATABASE` should be used to specify the SQLAlchemy connection string.

## Web UI

The root `/` displays a simple web interface when accessed by a browser that lists reservations and leases, and allows basic functionality to add/remove a lease.

![Simple Web UI](/webui.png "Web UI")

## API Notes

Read the API usage nodes at `/docs`.

## Motivation

At the moment only two functionalities are targetted, for IPv4 **only**. If there is interest maybe more.

- [x] List Leases
- [x] List / Add / Remove Reservations

The reasoning for this is simply to be able to quickly interact with the kea database backend to manipulate the backend data of kea in real-time, for automation purposes.

## Why not kea-ctrl-agent

The kea-ctrl-agent has limited use unless there is a support contract. I would suggest that a support contract for kea is worthwhile if you require it, this project should be seen as more of a proof of concept, that it can be done.

## Potential Issues

SQLAlchemy provides automap, which is going to be used to speed up development, however it may require to write glue-classes in Pydantic for ease of interaction with FastAPI and all its features.


## Milestones and future

The first steps of this project aims to achieve bare-minimum to be useful, its still in exploratory phase, meaning that the class-naming conventions may change. Support for dhcpv6 is not planned, however it would be prudent to do an oversight into the naming conventions once the project reaches bare-minimum usability.