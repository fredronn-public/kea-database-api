from setuptools import setup

with open('requirements.txt','r') as f:
    REQS=f.readlines()

setup(
    name='kea-database-api',
    version='0.2',
    install_requires=REQS,
    packages=[
        'kea_database_api'
    ],
    package_data={'kea_database_api':['templates/*','static/*']},
    entry_points='''
        [console_scripts]
        kea_database_api=kea_database_api:main
        kea_database_cli=kea_database_api.cli:cli
    ''',
)


