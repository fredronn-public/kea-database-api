$(document).ready(function(){
    // Delete 
    $('.delete').click(function(){
        var deleteid = $(this).data('id');
        var row = $(this).closest('tr');
        var confirmalert = confirm("Are you sure?");
        if (confirmalert == true) {
            // AJAX Request
            $.ajax({
                url: '/reservations/' + deleteid,
                type: 'DELETE',
                data: null,
                success: function(){
                    row.animate({ backgroundColor: 'red' }, 300)
                    .fadeOut(500, function() {
                      $(this).remove();
                    });
                },
                error: function(){alert("Invalid ID!");}
            });
        }
    });
});

function insertReservation(table, data) {
    var newRow = '<tr>' +
      '<td>' + data.host_id + '</td>' +
      '<td>' + data.hostname + '</td>' +
      '<td>' + data.dhcp_identifier + '</td>' +
      '<td>' + data.ipv4_address + '</td>' +
      '<td><button class="delete btn btn-danger" data-id="' + data.host_id + '">X</button></td>' +
      // Add more table cells as needed based on the properties of the object
      '</tr>';

    // Append the new row to the table body
    $(table).append(newRow);
}

$(function() {
    $('input').on('keypress', function(e) {
        if ( e.which === 13 ){
            var tbody = $(this).closest('table').children('tbody');
            var form =  $(this.form)
            $.ajax({
                url:form.data('url'),
                type:"POST",
                data:JSON.stringify(form.serializeJSON()),
                contentType:"application/json; charset=utf-8",
                dataType:"json",
                success: function(response){
                    insertReservation(tbody, response);
                },
                error: function(response){alert("Invalid format: " + response.message);}
            });
        }
    });
});
