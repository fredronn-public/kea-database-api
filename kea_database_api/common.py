from .models import session
def get_db():
    try:
        yield session
    except:
        session.rollback()
    finally:
        session.commit()
