#!/usr/bin/env python3
from typing import Optional
import json
import socket
import struct
import uvicorn
from .models import Lease4, Host, Session, session, select
import traceback
import os
from pathlib import Path

from fastapi import FastAPI, Request, Depends, status
from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles
from fastapi.responses import PlainTextResponse, HTMLResponse, JSONResponse

app = FastAPI()

templates = Jinja2Templates(directory=Path(os.path.dirname(__file__)).joinpath('templates'))
app.mount("/static", StaticFiles(directory=Path(os.path.dirname(__file__)).joinpath('static')), name="static")

def get_db():
    try:
        yield session
    except:
        session.rollback()
    finally:
        session.commit()

@app.exception_handler(Exception)
async def test(req, exc):
    #traceback.print_exc()
    return JSONResponse(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, content={"message": str(exc)})

@app.get("/", response_class=HTMLResponse)
async def list_leases_html(
        request: Request,
        db: Session = Depends(get_db)):
    return templates.TemplateResponse("root.html", {
        "request": request,
        "leases": db.exec(select(Lease4)).all(),
        "hosts": db.exec(select(Host)).all(),
    })

@app.get("/leases/")
async def list_leases(
        db: Session = Depends(get_db)) -> list[Lease4]:
    return db.exec(select(Lease4)).all()

@app.get("/reservations/")
async def list_reservations(
        db: Session = Depends(get_db)) -> list[Host]:
    return db.exec(select(Host)).all()

@app.post("/reservations/")
async def create_reservation(
        host: Host, db: Session = Depends(get_db)):
    db.add(host)
    db.commit()
    db.refresh(host)
    return host

@app.delete("/reservations/{host_id}")
async def delete_reservation(
        host_id: int, db: Session = Depends(get_db)):
    reservation = db.exec(select(Host).where(Host.host_id == host_id)).one()
    return db.delete(reservation)

def main():
    uvicorn.run("kea_database_api:app", host="0.0.0.0", port=8000, reload=True, log_level="info")

if __name__ == "__main__":
    sys.exit(main())
