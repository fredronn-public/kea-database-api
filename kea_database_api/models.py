from typing import Optional
from datetime import datetime
from sqlmodel import Field, Session, SQLModel, create_engine, select
from sqlalchemy.types import TypeDecorator, Integer, LargeBinary
from sqlalchemy import Column, create_engine
from pydantic import ValidationError, validator
import socket, struct, textwrap, binascii, macaddress
from os import getenv

# engine, suppose it has two tables 'user' and 'address' set up
engine = create_engine(getenv('KEA_DATABASE'))
session = Session(engine)

# Kea stores addresses as Integers, this is a TypeDecorator
# to help with converting to/from
# TODO: validation
class IPv4Address(TypeDecorator):
    impl = Integer
    def process_result_value(self, value, dialect):
        return socket.inet_ntoa(struct.pack('!L', value))
    def process_bind_param(self, value, dialect):
        return struct.unpack("!L", socket.inet_aton(value))[0]

class MacAddress(TypeDecorator):
    impl = LargeBinary
    def process_result_value(self, value, dialect):
        return str(macaddress.MAC(value))
    def process_bind_param(self, value, dialect):
        return bytes(macaddress.MAC(value))

class Lease4(SQLModel, table=True):
    __tablename__ = 'lease4'
    address: int = Field(sa_column=Column(IPv4Address, primary_key=True))
    hostname: str
    expire: datetime
    hwaddr: bytes = Field(sa_column=Column(MacAddress))
    subnet_id: int

class Host(SQLModel, table=True):
    __tablename__ = 'hosts'
    host_id: Optional[int] = Field(default=None, primary_key=True)
    hostname: Optional[str]
    dhcp_identifier: str = Field(sa_column=Column(MacAddress))
    dhcp_identifier_type: int = Field(default=0)
    ipv4_address: Optional[str] = Field(sa_column=Column(IPv4Address))
    dhcp4_subnet_id: int


