#!/usr/bin/env python3
from typing import Optional
from typing_extensions import Annotated
from fastapi import Depends
import typer
import re
from .models import Lease4, Host, select, session as db

cli = typer.Typer()

def pretty_print(result):
    for i in result: print(i)

@cli.command()
def hosts(hostname: Optional[str] = None):
    statement = select(Host)
    if hostname: statement = statement.where(Host.hostname.like('%{hostname}%'))
    pretty_print(db.exec(statement).all())

@cli.command()
def add_host(subnet: int, ip: str, mac: str, hostname: str):
    session.add(Host(dhcp_identifier=mac, hostname=hostname, ipv4_address=ip, dhcp4_subnet_id=subnet))

@cli.command()
def leases(hostname: Optional[str] = None):
    statement = select(Lease4)
    if hostname: statement = statement.where(Lease4.hostname.like(f'%{hostname}%'))
    pretty_print(db.exec(statement).all())

if __name__ == "__main__":
    app()
