let
    mach-nix = import (builtins.fetchGit {
        url = "https://github.com/DavHau/mach-nix";
    }) { python = "python310"; };
    package = (import ./derivation.nix);
    image = mach-nix.mkDockerImage {
        packagesExtra = [
            package
        ];
    };
in
    image.override (oldAttrs: {
        name = "kea-database-api";
        config.Cmd = [ "${package}/bin/kea_database_api" ];
    })
